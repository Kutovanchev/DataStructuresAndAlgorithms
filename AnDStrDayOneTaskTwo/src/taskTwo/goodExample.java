package taskTwo;

public class goodExample extends example {
	int key;
	
	public goodExample(int key) {
		
		this.key = key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + key;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		goodExample other = (goodExample) obj;
		if (key != other.key)
			return false;
		return true;
	}
}
