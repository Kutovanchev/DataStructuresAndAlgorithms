package taskTwo;

public class mediocreExample extends example {
	int key;
	
	public mediocreExample(int key) {
		this.key = key;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + key;
		return key*2-1+17-4^2;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		mediocreExample other = (mediocreExample) obj;
		if (key != other.key)
			return false;
		return true;
	}

}
