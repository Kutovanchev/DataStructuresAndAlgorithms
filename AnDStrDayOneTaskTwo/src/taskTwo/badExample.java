package taskTwo;

public class badExample extends example {
	int key;
	
	public badExample(int key) {
		this.key = key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + key;
		return 5;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		badExample other = (badExample) obj;
		if (key != other.key)
			return false;
		return true;
	}
	

}
