package taskTwo;

import java.util.ArrayList;
import java.util.HashMap;

public class TaskTwoMainn {

	public static ArrayList<Long> fill(HashMap<goodExample, Integer> gMap, HashMap<mediocreExample, Integer> mMap,
			HashMap<badExample, Integer> bMap) {
		
		ArrayList<Long> times = new ArrayList<Long>();
		
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			
			goodExample goodEx = new goodExample(i);
			gMap.put(goodEx, i);

		}
		long stop = System.currentTimeMillis();
		
		times.add(stop-start);
		
		start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {

			mediocreExample medEx = new mediocreExample(i);
			mMap.put(medEx, i);

		}
		stop = System.currentTimeMillis();
		
		times.add(stop-start);
		
		start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			badExample badEx = new badExample(i);
			bMap.put(badEx, i);					
		}
		stop = System.currentTimeMillis();
		
		times.add(stop-start);
		
		
		return times;
		
	}

	public static void main(String[] args) {

		HashMap<badExample, Integer> bMap = new HashMap<>();
		HashMap<mediocreExample, Integer> mMap = new HashMap<>();
		HashMap<goodExample, Integer> gMap = new HashMap<>();

		ArrayList<Long> times = fill(gMap, mMap, bMap);
		
		System.out.println("time for the good one: " + times.get(0));
		System.out.println("time for the mediocre one: " + times.get(1));
		System.out.println("time for the bad one: " + times.get(2));
		

	}

}
