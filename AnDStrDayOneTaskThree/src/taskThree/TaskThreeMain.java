package taskThree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class TaskThreeMain {
	
	public static ArrayList<Long> fillHashMaps (HashMap<Integer, Integer> hMap1, HashMap<Integer, Integer> hMap2, HashMap<Integer, Integer> hMap3) {
		Random random = new Random();
		ArrayList<Long> times = new ArrayList<>();
		
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			hMap1.put(random.nextInt(20000), random.nextInt(20000));
		}
		long stop = System.currentTimeMillis();
		
		times.add(stop-start);
		
		start = System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			hMap2.put(random.nextInt(200000), random.nextInt(200000));
		}
		stop = System.currentTimeMillis();
		
		times.add(stop-start);
		
		start = System.currentTimeMillis();
		for (int i = 0; i < 1000000; i++) {
			hMap3.put(random.nextInt(2000000), random.nextInt(2000000));
		}
		stop = System.currentTimeMillis();
		
		times.add(stop-start);
		
		return times;
	}
	
	public static void fillConcMap (ConcurrentHashMap<Integer, Integer> cMap, Integer size) {
		Random random = new Random();
		while (cMap.size()<size) {
			cMap.put(random.nextInt(size*2), random.nextInt(size*2));
		}
	}

	public static void main(String[] args) {
		
		HashMap<Integer, Integer> hMap1 = new HashMap<Integer,Integer>();
		HashMap<Integer, Integer> hMap2 = new HashMap<Integer,Integer>();
		HashMap<Integer, Integer> hMap3 = new HashMap<Integer,Integer>();
		
		ConcurrentHashMap<Integer, Integer> cMap1 = new ConcurrentHashMap<Integer, Integer>();
		ConcurrentHashMap<Integer, Integer> cMap2 = new ConcurrentHashMap<Integer, Integer>();
		ConcurrentHashMap<Integer, Integer> cMap3 = new ConcurrentHashMap<Integer, Integer>();
		
		ArrayList<Long> hMapTimes = fillHashMaps(hMap1, hMap2, hMap3);
		ArrayList<Long> cMapTimes = new ArrayList<>();
		
		long start = System.currentTimeMillis();
		Runnable task = () -> fillConcMap(cMap1,10000);
		new Thread(task).start();
		new Thread(task).start();
		new Thread(task).start();
		while (cMap1.size()<10000){
			;
		}
		
		long stop = System.currentTimeMillis();
		
		cMapTimes.add(stop-start);
		
		start = System.currentTimeMillis();
		task = () -> fillConcMap(cMap2, 100000);
		new Thread(task).start();
		new Thread(task).start();
		new Thread(task).start();
		while (cMap2.size()<100000){
			;
		}
		
		stop = System.currentTimeMillis();
		
		cMapTimes.add(stop-start);
		
		start = System.currentTimeMillis();
		task = () -> fillConcMap(cMap3, 1000000);
		new Thread(task).start();
		new Thread(task).start();
		new Thread(task).start();
		while (cMap3.size()<1000000){
			;
		}
		
		stop = System.currentTimeMillis();
		
		cMapTimes.add(stop-start);
		
		
		System.out.println("Time for HashMap with 10k: " + hMapTimes.get(0));
		System.out.println("Time for HashMap with 100k: " + hMapTimes.get(1));
		System.out.println("Time for HashMap with 1m: " + hMapTimes.get(2));
		
		System.out.println("Time for ConcMap with 10k: " + cMapTimes.get(0));
		System.out.println("Time for ConcMap with 100k: " + cMapTimes.get(1));
		System.out.println("Time for ConcMap with 1m: " + cMapTimes.get(2));
		
		
	}

}
